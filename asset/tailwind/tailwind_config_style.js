tailwind.config = {
    theme: {
      extend: {
        fontFamily: {
          'lobsterfont': "lobster",
      },
      backgroundImage: {
        'hero': "url(/asset/img/bg6.jpg)",
      },
      backgroundColor: {
        'green': '#3bb93b',
        'pink': '#FFB6C1',
        'lightPink': '#FFE4E1',
        'darkBlueGreen': '#3dddb5',
        'lightBlueGreen': '#7debd385',
        'black': '#1d1d1db2',
      }
      }
    }
}